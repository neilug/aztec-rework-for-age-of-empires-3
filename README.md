# **Aztec Rework - [ ] version 0.0.4**

## DISCLAIMER - Please Read

> Numbers aren't definitive and probably not well balanced yet. Most of numbers are arbitrary_

## Big Thanks to SquidTheSid

I had a lot of exchange with great members of the community, and I'm applying most of the changes from **[SquidTheSid](https://github.com/SquidTheSid/Age-Of-Empires-3-Rebalance-Mod/blob/master/patchnotes.txt)**.  

## **Updates**

---

### [ Jaguar Prowl Knight ]

>_From heavy infantry with 3 counter to light infantry : melee skirmisher ! Similar to the Urumi, this idea has been imagine by SquidTheSid._

- [x] HP: Lowered to 210 from 240

- [x] Siege: Lowered to 24 Siege Damage from 36

- [x] Armor: Changed from 10% melee to 20% ranged

- [x] 0.75x multiplier vs Cavalry and CoyoteMan added (with a further 0.75x vs light infantry), 2.0x light cavalry multiplier/xpEagleKnight multipliers added

- [x] Unit type is changed from **[HeavyInfantry]** to **[LightInfantry]**

---

### [ Eagle Runner Knight ]

> _Using again the idea behind SquidTheSid's balance patch : Due to the increased versatility of the Aztec army, ERKs have had their power levels slightly brought down in general, but are better vs artillery. (Incorporating Garja's changes)_

- [x] 1.25x AbstractArtillery damage multiplier added.

- [x] Armor: Reduced from 30% ranged to 20%

- [x] Speed : Increased from 6.0 to 6.25

---

### [ SkullKnight ]

> _SkullKnights are now produceables from the Temple ! Most of the numbers and balance has been made by SquidTheSid._

- [x] Cost : changes from **[ 250 ![alt text](Images/Coin.png "Coin") ]** to **[ 100 ![alt text](Images/Food.png "Food") / 150 ![alt text](Images/Coin.png "Coin") ]**  

- [x] Upgrades : You need to research **Champion** and **Legendary** upgrades,  avaiable in the new **[Temple]**

- [x] Armor: Changed from 20% melee to 20% ranged

- [x] 3x Cavalry and 2x Light Infantry damage multipliers removed (no penalties/strengths vs. any unit now)

- [x] Speed: Increased from 4.0 to 5.0, maximum speed increased from 6.0 to 7.5

- [x] Siege: Reduced to 20 damage from 72; now affected by **[ScorchedEarth]** shipment

- [x] Infantry and Heavy Infantry unit tags removed, Light Infantry and CoyoteMan unit tags added

- [x] Damage Cap: Reduced to 42 from 68

- [x] can now be produced in **[Temple]**.

---

### [ Arrow Knights ]

> _The buffs are meant to have proper siege damage since of the reworks of the SkullKnights and the Jaguar Prown Knights. They have better stats to survive canon's shot, justifying their 2 population cost. This change has been made by SquidTheSid._

- [x] cost changes from **[ 50 ![alt text](Images/Food.png "Coin") / 75 ![alt text](Images/Coin.png "Coin") ]** to **[ 70 ![alt text](Images/Food.png "Food") / 100 ![alt text](Images/Coin.png "Coin") ]**

- [x] HP: Increased to 160 from 150

- [x] Artillery Damage multiplier increased to 8x from 5x

- [x] Movement Speed: Increased from 3.75 to 4

- [x] Siege: Increased to 50 from 36

---

### [ WarriorPriest ]

- [x] cost changes from **[ 200 ![alt text](Images/Coin.png "Coin") ]** to **[ 80 ![alt text](Images/Food.png "Food") / 120 ![alt text](Images/Coin.png "Coin") ]**  

- [x] can now be produced in **[Temple]**

- [ ] can only be produced one by one.

---

## [ Shipments ]

- [x] Remove **[CoyoteRunner]**'s furtivity gain by shipment. It is now a technology to research, **[NocheTriste]**.

- [ ] _GoldenCities_ : deliver a **[Temple]** construction and increase maximum build limit by 1 for Age 4. Icon set to Tour de pierres.

- [ ] Triple Entente : similar to Inca's American Allies, give access to Zapotec & Mayans for Age 1.

- [x] **[QuetzalcoatlTempleSupport]** now ships 11 JPK instead of 18; cost reduced to 1000 gold.

> _Will be updated in further releases_

---

## News

### [ Temple ]

Building, Maximum of 2.

> _Note : Aztecs have already access to Nobles Hut, a strong defensive building. They don't need a stronghold. So I went to an approch of an economic/military building, with unique production and more researches, bringing fun and crucial decision to the game._

- [x] Each Temple generates **[ 3 ![alt text](Images/Coin.png "Coin") ]** per second.  

    > _Note : Since of the rework, Aztecs will require way more gold than before. This is probably interesting enough for either the player to defend those buildings, or the enemy to destroy them in first place._

- [x] Cost set to **[ 150 ![alt text](Images/Food.png "Food") / 250 ![alt text](Images/Wood.png "Wood") / 450 ![alt text](Images/Coin.png "Coin") ]**

- [x] Temple will get 1 upgrade for its HP in Age 4

- [x] Big Button **[MexicasWillPower]** : Reduce training Time of all aztec's infantry by 33%.  

> _Note : either the value is too strong, or the cost is too high. This value will probably change, since the dance of fertility already gives a lot of reduce time. My goal is to avoid depending of this dance in lategame, since you should have better dances to do._

Age 3 :

- [x] Allows the production of **[SkullKnight]** and **[WarriorPriest]**.

- [x] Allows the research of **[SonOfQuetzacoatl]** and **[NocheTriste]**.

- [x] Allows the research of **[TlalocBenediction]** or **[TlalocAbundance]**

Age 4 :

- [x] Allows the upgrade of **[ChampionSkullKnight]**

- [x] Allows the research of **[MightyTemple]** to increase Temple HP by 30 %.

- [x] Allows the research of **[TezcatlipocaMassacre]** or **[HuitzilopochtliProtection]**

> _Note : Choice Technology is a technology where you chose either A or B, kind of a reference to Age of Mythology. This will be unique to Age of Empires 3 and maybe isn't in the spirit of the game. But I still like the idea, whereas you can choose whatever you need, but you will be permanently affected by this, with no coming back. Since it also has a malus side, looking for making them free to research. Another way would be reducing their cost and removing the malus_

Age 5 :

- [x] Allows the upgrade of **[LegendarySkullKnight]**

---

### [ Son of Quetzacoatl ]

Technology, avaiable in age 3.  
**[ 550 ![alt text](Images/Food.png "Food") ]**

- [x] Increases RangedArmor by 0.2 for the **[Warchief]**.

- [ ] Active of Cauchic Ascension also grants a huge HP regeneration over 10seconds to the Warchief. Units affected by the Aura are affected but by a lesser value.

- [x] Placeholder Image set to Ixtilton

    > _Note : This upgrade might see some rework, but I like the idea of the Aztec's warchief being again a huge threat._

---

### [ Mexica's Will Power ]

Big Button, avaiable in age 4.  
**[ 750 ![alt text](Images/Coin.png "Coin") ]**

- [x] Reduce training time of all Aztec infantry by 33% (even **[SkullKnights]**).

- [x] Placeholder Image set to Maya Calendar

---

### [ Noche Triste ]

Technology, avaiable in age 3.  
**[ 600 ![alt text](Images/Food.png "Food") / 200 ![alt text](Images/Coin.png "Coin") ]**

- [x] The stealth speed malus is now only 20% for the **[CoyoteRunner]** and the **[JaguarKnight]**. Enable **[CoyoteRunner]** furtivity.

- [x] Placeholder Image set to Frappe Silencieuse

---

### [ Tezcatlipoca Massacre ]

Choice Technology, avaiable in age 4.

- [x] Cost set to **[ 400 ![alt text](Images/Food.png "Food") ]** and **[ 200 ![alt text](Images/Wood.png "Wood") ]**

- [x] All Military Units get **[ +25% attack ]** but lose **[ -10% HitPoints ]**. Disable **[HuitzilopochtliProtection]**.

- [x] Placeholder Image set to Cult Of The Dead

### [ Huitzilopochtli Protection ]

Choice Technology, avaiable in age 4.

- [x] Cost set to **[ 200 ![alt text](Images/Food.png "Food") ]** and **[ 400 ![alt text](Images/Wood.png "Wood") ]**

- [x] All Military Units get **[ +25% HitPoints ]** but lose **[ -10% attack ]**. Disable **[TezcatlipocaMassacre]**.

- [x] Placeholder Image set to Maya Cotton Armor

---

### [ Tlaloc Benediction ]

Choice Technology, avaiable in age 3.

- [x] Cost set to **[ 350 ![alt text](Images/Wood.png "Wood") ]** and **[ 350 ![alt text](Images/Coin.png "Coin") ]**

- [x] **[Farm]** gets **[ +25% rate ]**, but **[Plantation]** gets **[ -10% rate ]**. Disable **[TlalocAbundance]**.

- [x] Placeholder Image set to Zapotec Food of the Gods

### [ Tlaloc Abundance ]

Choice Technology, avaiable in age 3.

- [x] Cost set to **[ 350 ![alt text](Images/Food.png "Food") ]** and **[ 350 ![alt text](Images/Wood.png "Wood") ]**

- [x] **[Plantation]** gets **[ +25% rate ]**, but **[Farm]** gets **[ -10% rate ]**. Disable **[TlalocBenediction]**.

- [x] Placeholder Image set to Zapotec Cloud People

---

## Notes / Todo

- [ ] Building construction of temple set to Fort Animation (see if possible to set)

Templates :  

**[ x ![alt text](Images/Food.png "Food") ]**
**[ x ![alt text](Images/Wood.png "Wood") ]**
**[ x ![alt text](Images/Coin.png "Coin") ]**
**[ x ![alt text](Images/Population.png "Population") ]**  
**[ x ![alt text](Images/Food.png "Food") / x ![alt text](Images/Coin.png "Coin") ]**